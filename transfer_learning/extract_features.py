# USAGE
# python extract_features.py --dataset ../datasets/animals/images \
# 	--output ../datasets/animals/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/caltech-101/images \
# 	--output ../datasets/caltech-101/hdf5/features.hdf5
# python extract_features.py --dataset ../datasets/flowers17/images \
#	--output ../datasets/flowers17/hdf5/features.hdf5
"""
--dataset
../datasets/flowers17/images
--output
./output/flowers-17/hdf5/flowers17_features.hdf5
--output-label-encoder
./output/flowers-17/flowers17_label_encoder.pkl
"""
# import the necessary packages
from tensorflow.keras.applications import VGG16
from tensorflow.keras.applications import imagenet_utils
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from sklearn.preprocessing import LabelEncoder
from pyimagesearch.io.hdf5datasetwriter import HDF5DatasetWriter
from imutils import paths
import numpy as np
import progressbar
import argparse
import random
import os
import pickle

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
	help="path to input dataset")
ap.add_argument("--test-dataset", required=False,
	help="if there is a separate test-dataset directory, it can be included and combined with dataset")

ap.add_argument("-o", "--output", required=True,
	help="path to output HDF5 file")
ap.add_argument("-b", "--batch-size", type=int, default=32,
	help="batch size of images to be passed through network")
ap.add_argument("-s", "--buffer-size", type=int, default=1000,
	help="size of feature extraction buffer")
ap.add_argument("-l", "--output-label-encoder", required=True, help='name of file for label encoder')

args = vars(ap.parse_args())

bs = args['batch_size']

# grab the list of images that well be describing then randomly shuffle them to allow for
# easy training and testing splits via array slicing during training time
print("Loading images")
imagePaths = list(paths.list_images(args['dataset']))

if "test_dataset" in args:
    # then there was an additional directory of test images which we want to include in the
    # model training
    imagePaths.extend(list(paths.list_images(args['test_dataset'])))

random.shuffle(imagePaths)

# extract the class labels from the image paths then encode the labels
label_names = [p.split(os.path.sep)[-2] for p in imagePaths]
le =LabelEncoder()
labels = le.fit_transform(label_names)

print(f"Saving Label Encoder: {args['output_label_encoder']}")
f = open(args['output_label_encoder'], "wb")
f.write(pickle.dumps(le))
f.close()

# Load the VGG16 network
print("Loading VGG16....")
# include_top=False, indicates that hte final fully-connected layer
# should NOT be included in the architecture
model = VGG16(weights="imagenet", include_top=False)

# initialize the HDF5 dataset writer, then store the class label names in the dataset
dataset = HDF5DatasetWriter((len(imagePaths), 512*7*7), args['output'], dataKey='features', bufSize=args['buffer_size'])
dataset.storeClassLabels(le.classes_)

# initialize the progress bar
widgets = ['Extracting Features: ', progressbar.Percentage(), " ", progressbar.Bar(), " ", progressbar.ETA()]
pbar = progressbar.ProgressBar(maxval=len(imagePaths), widgets=widgets).start()

# loop over the images in batches
for i in np.arange(0, len(imagePaths), bs):
    # extract the batch of images and labels, then intialize the list of actual
    # images that will be passed through the network
    # for feature extraction
    batchPaths = imagePaths[i : i+bs]
    batchLabels = labels[i : i+bs]
    batchImages = []

    for (j, imagePath) in enumerate(batchPaths):
        # load the input image using the Keras Helper utility
        # while ensuring the image is resized to 224x224 pixels
        image = load_img(imagePath, target_size=(224,224))
        image = img_to_array(image)

        # preprocess the image by
        # (1) expanding the dimensions
        # (2) subtracting the mean RGB pixel intensity from the ImageNet dataset
        image = np.expand_dims(image, axis=0)
        image = imagenet_utils.preprocess_input(image)

        batchImages.append(image)
    # pass the batch sized images through the network and use the outputs
    # as our actual features
    # vertifically stack images such that we have a shape like
    # (N, 224, 224, 3)
    batchImages = np.vstack(batchImages)
    features = model.predict(batchImages, batch_size=bs)

    # reshape the features so that each image is represented by a flattened feature vector
    # of the 'MexPooling2D' outputs
    features = features.reshape((features.shape[0], 512*7*7))

    # add the features and labels to our HDF5 dataset
    dataset.add(features, batchLabels)
    pbar.update(i)

# close the dataset
dataset.close()
pbar.finish()
