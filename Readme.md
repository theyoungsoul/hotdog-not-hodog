# Keras / TensorFlow Hotdog, Not Hotdog Classifier

![HDNHD](./readme_images/HD_NHD.gif)

The inspiration for putting together this project was reading the book:

[PyImageSearch DeepLearning for Computer Vision](https://www.pyimagesearch.com/deep-learning-computer-vision-python-book/)

One of the last projects in the starter bundle is a 'smiling', 'not smiling' project.  I wanted to take what I had learned in that book, along with the what I was learning in the Practioner Bundle, and apply it to a different dataset.

The dataset I decided to try was 'hotdog', 'not hotdog' image classifier made famous by the series Silicon Valley.  You can see a short clip of the scene here:

https://www.youtube.com/watch?v=ACmydtFDTGs

This repo is to show how to build image classifiers in Keras using Convolutional Neural Networks.
We will build a Convolutional Neural Network from scratch and show how to use transfer learning.


## Dataset

Initially the dataset came from the [Kaggle hot-dog-not-hot-dog dataset](https://www.kaggle.com/dansbecker/hot-dog-not-hot-dog/data)

However, as I was training models and found that the accuracy was not very good and I really needed more images to work with.

I then read this [blog](https://towardsdatascience.com/building-the-hotdog-not-hotdog-classifier-from-hbos-silicon-valley-c0cb2317711f) which described how they downloaded data from the ImageNet dataset.

In the end I combined data from the Kaggle Dataset and the ImageNet Dataset.

Depending upon how many images you are able to download from the ImageNet dataset, you should have around 6400 total images from ImageNet and about 1000 from the Kaggle dataset.


### Download ImageNet Data

See the file, get_imagenet_data.py.  This will file takes a single argument for the root output directory to store the imagenet images and it will download the following categories of images:

`
'pets', 'furniture', 'people', 'food', 'frankfurter', 'chili-dog', 'hotdog'
`

### Download the Kaggle Dataset

Download the [Kaggle hot-dog-not-hot-dog dataset](https://www.kaggle.com/dansbecker/hot-dog-not-hot-dog/data)

### Create Datasets

Using data from both Kaggle and ImageNet create a training, testing and holdout set of images.

See file, `create_train_test_holdout_dataset.py`

A typical usage would be: `create_dataset.sh`

```bash
python create_train_test_holdout_dataset.py --output-path /Volumes/MacBackup/delete-derived-hotdog --imagenet-path /Volumes/MacBackup/delete-hotdog --seefood-path /Volumes/MacBackup/hotdog/Kaggle --train-size 1000 --test-size 400 --holdout-size 200
```

### Curated Dataset

You can find a dataset with training, testing and holdout at the following github link:

https://github.com/youngsoul/hotdog-not-hotdog-dataset

Or use the scripts above to create your own.


## Training a Model

I tried 3 different network architectures:

* MiniVGGNet

* LeNet

* Custom Architecture arrived at by reading Kaggle Notebooks

The one that was the best balance between training time and accuracy was the custom architecture.

You can find that one in:

`pyimagesearch/conv/hotdogconvnet.py`

### Epochs 100

Summary of training:
```text
Epoch 00100: val_accuracy did not improve from 0.89500
Using train datasets: /Volumes/MacBackup/derived_dataset/train
Using test datasets: /Volumes/MacBackup/derived_dataset/test
Total training time for 100 Epochs, ImageSize=128, Train Images=4298, Test Images: 400: 104.6997279882431 minutes
_________________________________________________________________
[INFO] evaluating network...
              precision    recall  f1-score   support

     hot_dog       0.87      0.93      0.90       100
 not_hot_dog       0.92      0.86      0.89       100

    accuracy                           0.90       200
   macro avg       0.90      0.90      0.89       200
weighted avg       0.90      0.90      0.89       200

            hotdog  not_hotdog
hotdog          86          14
not_hotdog       7          93
Holdout Accuracy: 0.895

```
![Epochs100](./readme_images/epochs_100.png)



### Epochs 200

Summary of the training:
```text
Using the model trained on 100, training for 100 more Epochs improved the model slightly.
Epoch 00100: val_accuracy did not improve from 0.93250
134/134 [==============================] - 65s 484ms/step - loss: 0.1645 - accuracy: 0.9330 - val_loss: 0.1890 - val_accuracy: 0.9175
Using train datasets: /Volumes/MacBackup/derived_dataset/train
Using test datasets: /Volumes/MacBackup/derived_dataset/test
Classes: ['hot_dog' 'not_hot_dog'], ['hot_dog' 'not_hot_dog']
Total training time for 100 Epochs, ImageSize=128, Train Images=4298, Test Images: 400: 108.13885193268457 minutes
[INFO] evaluating network with best model...
              precision    recall  f1-score   support

     hot_dog       0.88      0.94      0.91       100
 not_hot_dog       0.94      0.87      0.90       100

    accuracy                           0.91       200
   macro avg       0.91      0.91      0.90       200
weighted avg       0.91      0.91      0.90       200

            hotdog  not_hotdog
hotdog          87          13
not_hotdog       6          94
Holdout Accuracy: 0.905

```

![Epochs200](./readme_images/epochs_200.png)


## Transfer Learning

I then used a VGG16 ConvNet with the imagenet weights to perform transfer learning and used that network as a feature extractor.  I used the resulting feature vector for all of the images with a LogisticRegression model.  Below are the results of that

```text
Best HyperParameters: {'C': 10.0}
evaluating
              precision    recall  f1-score   support

     hot_dog       0.92      0.95      0.93       645
 not_hot_dog       0.93      0.90      0.92       530

    accuracy                           0.93      1175
   macro avg       0.93      0.92      0.92      1175
weighted avg       0.93      0.93      0.92      1175

Saving Model: ./hotdog/hotdog_model.pkl

```

You can see that this approach produced the best overall results, and was by far the fastest route to getting an image classifier working.


## Reference Links

https://www.pyimagesearch.com

https://www.kaggle.com/dansbecker/hot-dog-not-hot-dog/data

https://www.kaggle.com/mjmurphy28/cnn-with-keras-accuracy-98

https://medium.com/hackernoon/building-a-serverless-not-hotdog-classifier-with-aws-sagemaker-and-lambda-dd473cf6506a

https://towardsdatascience.com/building-the-hotdog-not-hotdog-classifier-from-hbos-silicon-valley-c0cb2317711f
