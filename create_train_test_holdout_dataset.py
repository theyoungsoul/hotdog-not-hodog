from pathlib import Path
from random import sample
import shutil
import argparse

"""
This script will create a directory structure which will contain a train, test, validate set of directories
with the specified number of images in each.

"""

train_dataset_size = 2000
test_dataset_size = 200
holdout_dataset_size = 100

hotdog_image_paths = None
not_hotdog_image_paths = None

output_hd_train = None
output_nhd_train = None
output_hd_test = None
output_nhd_test = None
output_hd_holdout = None
output_nhd_holdout = None


def create_output_directories():
    Path(output_hd_train).mkdir(parents=True, exist_ok=True)
    Path(output_nhd_train).mkdir(parents=True, exist_ok=True)
    Path(output_hd_test).mkdir(parents=True, exist_ok=True)
    Path(output_nhd_test).mkdir(parents=True, exist_ok=True)
    Path(output_hd_holdout).mkdir(parents=True, exist_ok=True)
    Path(output_nhd_holdout).mkdir(parents=True, exist_ok=True)

def get_list_of_files(path_list, number_train, number_test, number_holdout):
    """
    :param: path_list - list of paths to images.
    :param: number_of_files - number of random sample files to return
    :return: list of Path objects representing the sampled files
    """
    all_files = []
    for path in path_list:
        p = Path(path).glob('**/*')
        files = [x for x in p if x.is_file()]
        all_files.extend(files)

    total_sample_size = number_train+number_test+number_holdout
    if len(all_files) < total_sample_size:
        raise ValueError(f"All Files: {len(all_files)} is less than requested: {total_sample_size}")

    random_sample = sample(all_files, total_sample_size)

    return random_sample[0:number_train], random_sample[number_train:(number_train+number_test)], random_sample[(number_train+number_test):]


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("--output-path", type=str, required=True, help="path to root where all dataset will be created")
    ap.add_argument("--imagenet-path", type=str, required=True, help="path to root where the imagenet data is located.  The contents of this directory should be the collection of folders from the get_imagenet_data script")
    ap.add_argument("--seefood-path", type=str, required=True, help="path to root where the files from the kaggle dataset is located.  The contents should of this directory should be a test and train folder.")
    ap.add_argument("--train-size", type=int, required=True, help="number of training images. there will be train-size hotdog images and train-size not-hot-dog images")
    ap.add_argument("--test-size", type=int, required=True, help="number of testing images. there will be test-size hotdog images and test-size not-hot-dog images")
    ap.add_argument("--holdout-size", type=int, required=True, help="number of holdout images. there will be holdout-size hotdog images and holdout-size not-hot-dog images")

    args = vars(ap.parse_args())

    train_dataset_size = args['train_size']
    test_dataset_size = args['test_size']
    holdout_dataset_size = args['holdout_size']

    imagenet_root = args['imagenet_path']
    seefood_root = args['seefood_path']

    hotdog_image_paths = [
        f"{imagenet_root}/chili-dog",
        f"{imagenet_root}/frankfurter",
        f"{imagenet_root}/hotdog",
        f"{seefood_root}/train/hot_dog",
        f"{seefood_root}/test/hot_dog"
    ]

    not_hotdog_image_paths = [
        f"{imagenet_root}/food",
        f"{imagenet_root}/furniture",
        f"{imagenet_root}/people",
        f"{imagenet_root}/pets",
        f"{seefood_root}/train/not_hot_dog",
        f"{seefood_root}/test/not_hot_dog"
    ]

    dataset_output_root_dir = args['output_path']
    output_hd_train = f"{dataset_output_root_dir}/train/hot_dog"
    output_nhd_train = f"{dataset_output_root_dir}/train/not_hot_dog"
    output_hd_test = f"{dataset_output_root_dir}/test/hot_dog"
    output_nhd_test = f"{dataset_output_root_dir}/test/not_hot_dog"
    output_hd_holdout = f"{dataset_output_root_dir}/holdout/hot_dog"
    output_nhd_holdout = f"{dataset_output_root_dir}/holdout/not_hot_dog"

    print(f"Creating derived dataset to directory: {dataset_output_root_dir}")
    print(f"Creating Training Size: {train_dataset_size} ")
    print(f"Creating Testing Size: {test_dataset_size} ")
    print(f"Creating Holdout Size: {holdout_dataset_size} ")

    create_output_directories()

    hd_train,hd_test,hd_holdout = get_list_of_files(hotdog_image_paths, train_dataset_size, test_dataset_size, holdout_dataset_size)
    nhd_train,nhd_test,nhd_holdout = get_list_of_files(not_hotdog_image_paths, train_dataset_size, test_dataset_size, holdout_dataset_size)

    for src in hd_train:
        shutil.copy(src, output_hd_train)

    for src in hd_test:
        shutil.copy(src, output_hd_test)

    for src in hd_holdout:
        shutil.copy(src, output_hd_holdout)

    for src in nhd_train:
        shutil.copy(src, output_nhd_train)

    for src in nhd_test:
        shutil.copy(src, output_nhd_test)

    for src in nhd_holdout:
        shutil.copy(src, output_nhd_holdout)


    print("Done....")