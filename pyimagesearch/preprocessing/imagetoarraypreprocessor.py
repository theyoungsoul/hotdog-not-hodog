# import the necessary packages
from tensorflow.keras.preprocessing.image import img_to_array
import numpy as np


class ImageToArrayPreprocessor:
    def __init__(self, dataFormat=None, scale_value=None, expand_dims=False):
        # store the image data format
        self.dataFormat = dataFormat
        self.scale_value = scale_value
        self.expand_dims = expand_dims

    def preprocess(self, image):
        # apply the Keras utility function that correctly rearranges
        # the dimensions of the image
        img_array = img_to_array(image, data_format=self.dataFormat)
        if self.scale_value:
            img_array = img_array / 255.0

        if self.expand_dims:
            img_array = np.expand_dims(img_array, axis=0)

        return img_array


if __name__ == '__main__':
    import argparse
    import cv2
    ap = argparse.ArgumentParser()
    ap.add_argument("--image", type=str, required=True, help="full path to image")

    args = vars(ap.parse_args())

    image = args['image']
    unresized_image = cv2.imread(image)

    p = ImageToArrayPreprocessor(scale_value=255.0, expand_dims=True)

    new_im = p.preprocess(unresized_image)

    print(new_im)

