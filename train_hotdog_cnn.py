from pyimagesearch.datasets.simpledatasetloader import SimpleDatasetLoader
from pyimagesearch.preprocessing.aspectawarepreprocessor import AspectAwarePreprocessor
from pyimagesearch.preprocessing.imagetoarraypreprocessor import ImageToArrayPreprocessor
import argparse
from sklearn.preprocessing import LabelEncoder
from pyimagesearch.conv.minivggnet import MiniVGGNet
from pyimagesearch.conv.lenet import LeNet
from pyimagesearch.conv.hotdogconvnet import HotDogConvNet
from tensorflow.keras.optimizers import SGD, Adam
from tensorflow.keras.callbacks import ModelCheckpoint
import numpy as np
from tensorflow.keras.utils import to_categorical
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report, confusion_matrix
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import time
import pandas as pd
from tensorflow.keras.models import load_model


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("--train-dataset", type=str, required=True, help="path to train dataset. directory should have hot_dog, not_hot_dog directories")
    ap.add_argument("--test-dataset", type=str, required=True, help="path to test dataset.  directory should have hot_dog, not_hot_dog directories")
    ap.add_argument("--holdout-dataset", type=str, required=True, help="path to holdout dataset.  directory should have hot_dog, not_hot_dog directories")
    ap.add_argument("--to-size", type=int, required=True, help="resize to square of size by size")
    ap.add_argument("--model", type=str, required=False, help="path to *specific* model checkpoint to load")

    args = vars(ap.parse_args())
    to_size = args['to_size']
    train_dataset_path = args['train_dataset']
    test_dataset_path = args['test_dataset']
    holdout_dataset_path = args['holdout_dataset']

    batch_size = 32
    epochs = 100

    p1 = AspectAwarePreprocessor(to_size, to_size)
    p2 = ImageToArrayPreprocessor(scale_value=255.0, expand_dims=False)

    dataset_loader = SimpleDatasetLoader([p1, p2])
    (train_data, train_labels, train_img_paths) = dataset_loader.load(train_dataset_path, verbose=50)
    (test_data, test_labels, test_img_paths) = dataset_loader.load(test_dataset_path, verbose=50)
    (holdout_data, holdout_labels, holdout_img_paths) = dataset_loader.load(holdout_dataset_path, verbose=50)

    le = LabelEncoder().fit(train_labels)
    X_train = train_data
    y_train = to_categorical(le.transform(train_labels), 2)

    le_test = LabelEncoder().fit(test_labels)
    X_test = test_data
    y_test = to_categorical(le_test.transform(test_labels), 2)
    # y_test = [ [1,0], [0,1] ] or list of hd, nhd]

    le_holdout = LabelEncoder().fit(holdout_labels)
    X_holdout = holdout_data
    y_holdout = to_categorical(le_holdout.transform(holdout_labels), 2)

    if args['model'] is None:
        model = HotDogConvNet.build(width=to_size, height=to_size, depth=3, classes=2)
        opt = SGD(lr=0.01, decay=0.01 / epochs, momentum=0.9, nesterov=True)
        # opt = Adam(lr=0.0001)
        model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
        best_model_name = "best_hotdog_cnn_model.h5"
    else:
        # load an existing model to continue the training with
        model = load_model(args['model'])
        best_model_name = "best_hotdog_cnn_model_100_200.h5"


    # setup model checkpoints to save best model
    checkpoint = ModelCheckpoint(best_model_name, monitor="val_accuracy", save_best_only=True, verbose=1)
    callbacks = [checkpoint]

    aug = ImageDataGenerator(rotation_range=30, width_shift_range=0.1, height_shift_range = 0.1, shear_range = 0.2, zoom_range = 0.2, horizontal_flip = True, fill_mode = "nearest")

    start = time.time()
    H = model.fit_generator(aug.flow(X_train, y_train, batch_size=batch_size), steps_per_epoch=len(X_train)//batch_size, validation_data=(X_test, y_test), epochs=epochs, verbose=1, callbacks=callbacks)
    end = time.time()


    print(f"Using train datasets: {train_dataset_path}")
    print(f"Using test datasets: {test_dataset_path}")
    print(f"Classes: {le.classes_}, {le_test.classes_}")

    print(f"Total training time for {epochs} Epochs, ImageSize={to_size}, Train Images={len(train_labels)}, Test Images: {len(test_labels)}: {(end-start)/60} minutes")


    try:
        model.summary()
    finally:
        pass
    try:
        # Load the best model and evaluate performace on the holdout dataset
        best_model = load_model(best_model_name)

        # evaluate the network
        print("[INFO] evaluating network with best model...")
        predictions = best_model.predict(X_holdout, batch_size=batch_size)
        print(classification_report(y_holdout.argmax(axis=1),
            predictions.argmax(axis=1), target_names=le_holdout.classes_))

        y_holdout_hd = []
        pred_hd = []
        for v in y_holdout:
            y_holdout_hd.append(v[0])

        for v in predictions:
            if v[0] > v[1]:
                pred_hd.append(1)
            else:
                pred_hd.append(0)

        cm = confusion_matrix(y_holdout_hd, pred_hd)
        df_cm = pd.DataFrame(
                cm, index=['hotdog', 'not_hotdog'], columns=['hotdog', 'not_hotdog'],
            )
        print(df_cm.head())

        tn = cm[0][0]
        tp = cm[1][1]
        fp = cm[0][1]
        fn = cm[1][0]

        print(f"Holdout Accuracy: {(tp+tn)/(tp+tn+fp+fn)}")
    except:
        pass

    # plot the training + testing loss and accuracy
    plt.style.use("ggplot")
    plt.figure()
    plt.plot(np.arange(0, epochs), H.history["loss"], label="train_loss")
    plt.plot(np.arange(0, epochs), H.history["val_loss"], label="val_loss")
    plt.plot(np.arange(0, epochs), H.history["accuracy"], label="acc")
    plt.plot(np.arange(0, epochs), H.history["val_accuracy"], label="val_acc")
    plt.title("Training Loss and Accuracy")
    plt.xlabel("Epoch #")
    plt.ylabel("Loss/Accuracy")
    plt.legend()
    plt.show()


"""
image size: 64
Total training time for 400 Epochs: 21.47491396665573 minutes
[INFO] evaluating network...
              precision    recall  f1-score   support

     hot_dog       0.72      0.56      0.63        95
 not_hot_dog       0.66      0.80      0.72       103

    accuracy                           0.68       198
   macro avg       0.69      0.68      0.67       198
weighted avg       0.69      0.68      0.68       198

HotDogConvNet
Total training time for 400 Epochs, ImageSize: 128: 77.49721936384837 minutes
[INFO] evaluating network...
              precision    recall  f1-score   support

     hot_dog       0.71      0.69      0.70        95
 not_hot_dog       0.72      0.74      0.73       103

    accuracy                           0.72       198
   macro avg       0.72      0.72      0.72       198
weighted avg       0.72      0.72      0.72       198


MiniVGGNet
Using datasets: ['/Volumes/MacBackup/hotdog/hot-dog-not-hot-dog/seefood/train', '/Volumes/MacBackup/hotdog/hot-dog-not-hot-dog/seefood/test']
Total training time for 100 Epochs, ImageSize=128, Total Images=988: 48.63227098385493 minutes
[INFO] evaluating network...
              precision    recall  f1-score   support

     hot_dog       0.74      0.69      0.72        95
 not_hot_dog       0.73      0.78      0.75       103

    accuracy                           0.74       198
   macro avg       0.74      0.74      0.74       198
weighted avg       0.74      0.74      0.74       198

MiniVGGNet
Using datasets: ['/Volumes/MacBackup/hotdog/hot-dog-not-hot-dog/seefood/train', '/Volumes/MacBackup/hotdog/hot-dog-not-hot-dog/seefood/test']
Total training time for 200 Epochs, ImageSize=128, Total Images=988: 102.34871178468069 minutes
[INFO] evaluating network...
              precision    recall  f1-score   support

     hot_dog       0.77      0.67      0.72        95
 not_hot_dog       0.73      0.82      0.77       103

    accuracy                           0.75       198
   macro avg       0.75      0.74      0.74       198
weighted avg       0.75      0.75      0.75       198

----------------------------------------------------------------
MiniVGGNet
Added a large number of images from ImageNet
Epoch 00200: val_accuracy did not improve from 0.82893
127/127 [==============================] - 157s 1s/step - loss: 0.2991 - accuracy: 0.8633 - val_loss: 0.4751 - val_accuracy: 0.8045
Using datasets: ['/Volumes/MacBackup/hotdog/hot-dog-not-hot-dog/seefood/train', '/Volumes/MacBackup/hotdog/hot-dog-not-hot-dog/seefood/test', '/Volumes/MacBackup/hotdog/hot-dog-not-hot-dog/seefood/imagenet']
Classes: ['hot_dog' 'not_hot_dog']
Total training time for 200 Epochs, ImageSize=128, Total Images=5114: 529.2343897859256 minutes
[INFO] evaluating network...
              precision    recall  f1-score   support

     hot_dog       0.83      0.73      0.77       470
 not_hot_dog       0.79      0.87      0.83       553

    accuracy                           0.80      1023
   macro avg       0.81      0.80      0.80      1023
weighted avg       0.81      0.80      0.80      1023


----------------------
Large Dataset
HotDogModel
Epoch 00100: val_accuracy did not improve from 0.90250
134/134 [==============================] - 67s 498ms/step - loss: 0.2876 - accuracy: 0.8754 - val_loss: 0.2697 - val_accuracy: 0.8675
Using train datasets: /Volumes/MacBackup/derived_dataset/train
Using test datasets: /Volumes/MacBackup/derived_dataset/train
Classes: ['hot_dog' 'not_hot_dog'], ['hot_dog' 'not_hot_dog']
Total training time for 100 Epochs, ImageSize=128, Train Images=4298, Test Images: 400: 111.77434950272242 minutes
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
conv2d (Conv2D)              (None, 126, 126, 32)      896       
_________________________________________________________________
max_pooling2d (MaxPooling2D) (None, 63, 63, 32)        0         
_________________________________________________________________
dropout (Dropout)            (None, 63, 63, 32)        0         
_________________________________________________________________
conv2d_1 (Conv2D)            (None, 61, 61, 64)        18496     
_________________________________________________________________
max_pooling2d_1 (MaxPooling2 (None, 30, 30, 64)        0         
_________________________________________________________________
dropout_1 (Dropout)          (None, 30, 30, 64)        0         
_________________________________________________________________
conv2d_2 (Conv2D)            (None, 28, 28, 128)       73856     
_________________________________________________________________
dropout_2 (Dropout)          (None, 28, 28, 128)       0         
_________________________________________________________________
flatten (Flatten)            (None, 100352)            0         
_________________________________________________________________
dense (Dense)                (None, 128)               12845184  
_________________________________________________________________
dropout_3 (Dropout)          (None, 128)               0         
_________________________________________________________________
dense_1 (Dense)              (None, 2)                 258       
=================================================================
Total params: 12,938,690
Trainable params: 12,938,690
Non-trainable params: 0
_________________________________________________________________
[INFO] evaluating network...
              precision    recall  f1-score   support

     hot_dog       0.85      0.88      0.86       100
 not_hot_dog       0.88      0.84      0.86       100

    accuracy                           0.86       200
   macro avg       0.86      0.86      0.86       200
weighted avg       0.86      0.86      0.86       200

            hotdog  not_hotdog
hotdog          84          16
not_hotdog      12          88
Holdout Accuracy: 0.86

From Client on holdout set
[INFO] evaluating network...
              precision    recall  f1-score   support

     hot_dog       0.83      0.93      0.88       100
 not_hot_dog       0.92      0.81      0.86       100

    accuracy                           0.87       200
   macro avg       0.88      0.87      0.87       200
weighted avg       0.88      0.87      0.87       200

            hotdog  not_hotdog
hotdog          81          19
not_hotdog       7          93
Holdout Accuracy: 0.87

--------------------------------------------------------------------------------------------------------
NOTES: 12/09/2019 Best model so far.  the loss/accuracy looked really good and it looked like there
was still more learning to be done if we let the number of epochs go.

Input Parameters:
--train-dataset
/Volumes/MacBackup/derived_dataset/train
--test-dataset
/Volumes/MacBackup/derived_dataset/test
--holdout-dataset
/Volumes/MacBackup/derived_dataset/holdout
--to-size
128


MODEL:  best_hotdog_cnn_model.h5  ID=12092019A
Epoch 00100: val_accuracy did not improve from 0.89500
Using train datasets: /Volumes/MacBackup/derived_dataset/train
Using test datasets: /Volumes/MacBackup/derived_dataset/test
Total training time for 100 Epochs, ImageSize=128, Train Images=4298, Test Images: 400: 104.6997279882431 minutes
_________________________________________________________________
[INFO] evaluating network...
              precision    recall  f1-score   support

     hot_dog       0.87      0.93      0.90       100
 not_hot_dog       0.92      0.86      0.89       100

    accuracy                           0.90       200
   macro avg       0.90      0.90      0.89       200
weighted avg       0.90      0.90      0.89       200

            hotdog  not_hotdog
hotdog          86          14
not_hotdog       7          93
Holdout Accuracy: 0.895


Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
conv2d (Conv2D)              (None, 126, 126, 32)      896       
_________________________________________________________________
max_pooling2d (MaxPooling2D) (None, 63, 63, 32)        0         
_________________________________________________________________
dropout (Dropout)            (None, 63, 63, 32)        0         
_________________________________________________________________
conv2d_1 (Conv2D)            (None, 61, 61, 64)        18496     
_________________________________________________________________
max_pooling2d_1 (MaxPooling2 (None, 30, 30, 64)        0         
_________________________________________________________________
dropout_1 (Dropout)          (None, 30, 30, 64)        0         
_________________________________________________________________
conv2d_2 (Conv2D)            (None, 28, 28, 128)       73856     
_________________________________________________________________
dropout_2 (Dropout)          (None, 28, 28, 128)       0         
_________________________________________________________________
flatten (Flatten)            (None, 100352)            0         
_________________________________________________________________
dense (Dense)                (None, 128)               12845184  
_________________________________________________________________
dropout_3 (Dropout)          (None, 128)               0         
_________________________________________________________________
dense_1 (Dense)              (None, 2)                 258       
=================================================================
Total params: 12,938,690
Trainable params: 12,938,690
Non-trainable params: 0

---------------------------------------------------------------------------------------------------
Using the model trained on 100, training for 100 more Epochs improved the model slightly.
Epoch 00100: val_accuracy did not improve from 0.93250
134/134 [==============================] - 65s 484ms/step - loss: 0.1645 - accuracy: 0.9330 - val_loss: 0.1890 - val_accuracy: 0.9175
Using train datasets: /Volumes/MacBackup/derived_dataset/train
Using test datasets: /Volumes/MacBackup/derived_dataset/test
Classes: ['hot_dog' 'not_hot_dog'], ['hot_dog' 'not_hot_dog']
Total training time for 100 Epochs, ImageSize=128, Train Images=4298, Test Images: 400: 108.13885193268457 minutes
[INFO] evaluating network with best model...
              precision    recall  f1-score   support

     hot_dog       0.88      0.94      0.91       100
 not_hot_dog       0.94      0.87      0.90       100

    accuracy                           0.91       200
   macro avg       0.91      0.91      0.90       200
weighted avg       0.91      0.91      0.90       200

            hotdog  not_hotdog
hotdog          87          13
not_hotdog       6          94
Holdout Accuracy: 0.905

 Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
conv2d (Conv2D)              (None, 126, 126, 32)      896       
_________________________________________________________________
max_pooling2d (MaxPooling2D) (None, 63, 63, 32)        0         
_________________________________________________________________
dropout (Dropout)            (None, 63, 63, 32)        0         
_________________________________________________________________
conv2d_1 (Conv2D)            (None, 61, 61, 64)        18496     
_________________________________________________________________
max_pooling2d_1 (MaxPooling2 (None, 30, 30, 64)        0         
_________________________________________________________________
dropout_1 (Dropout)          (None, 30, 30, 64)        0         
_________________________________________________________________
conv2d_2 (Conv2D)            (None, 28, 28, 128)       73856     
_________________________________________________________________
dropout_2 (Dropout)          (None, 28, 28, 128)       0         
_________________________________________________________________
flatten (Flatten)            (None, 100352)            0         
_________________________________________________________________
dense (Dense)                (None, 128)               12845184  
_________________________________________________________________
dropout_3 (Dropout)          (None, 128)               0         
_________________________________________________________________
dense_1 (Dense)              (None, 2)                 258       
=================================================================
Total params: 12,938,690
Trainable params: 12,938,690
Non-trainable params: 0
_________________________________________________________________

"""