import itertools
import os
import urllib
import urllib.request
from multiprocessing.dummy import Pool as ThreadPool
import cv2
from pathlib import Path
import requests
from PIL import Image
from io import BytesIO
import argparse

"""
This script will download data from the ImageNet data set and then remove an zero length files.
This script will also log any invalid links, so the next time it runs it can skip invalid linkes.
"""

pic_num = 1
root_path = None


def store_raw_images(paths, links):
    global pic_num
    for link, path in zip(links, paths):
        if not os.path.exists(path):
            os.makedirs(path)
        image_urls = str(urllib.request.urlopen(link).read())

        pool = ThreadPool(32)
        pool.starmap(loadImage, zip(itertools.repeat(path), image_urls.split('\\n'), itertools.count(pic_num)))
        pool.close()
        pool.join()


def loadImage(path, link, counter):
    global pic_num

    full_path = f"{root_path}/{path}"
    Path(full_path).mkdir(parents=True, exist_ok=True)
    if pic_num < counter:
        pic_num = counter + 1
    try:
        image_file_name = f"imagenet_{path}_{counter}.jpg"
        full_image_file_path = f"{full_path}/{image_file_name}"
        link = link.replace("\\r", "")
        print(f"Get Link: {link}")
        r = requests.get(link, stream=True, timeout=30)

        image_bytes = BytesIO(r.content)

        i = Image.open(image_bytes)

        i.save(full_image_file_path)

        # urllib.request.urlretrieve(link, full_image_file_path)
        img = cv2.imread(full_image_file_path)
        if img is not None:
            cv2.imwrite(full_image_file_path, img)
            print(counter)
        else:
            print(f"Cannot read image: {full_image_file_path}")

    except Exception as e:
        print(f"{str(e)}: {link}")


def removeInvalid(dirPaths):
    for dirPath in dirPaths:
        full_path = f"{root_path}/{dirPath}"

        for img in os.listdir(full_path):
            p = f"{full_path}/{img}"
            s = Path(p).stat().st_size
            if s == 0:
                Path(p).unlink()
                print(f"Delete File: {img}, zero length file")


def main():
    global root_path
    ap = argparse.ArgumentParser()
    ap.add_argument("--output-path", type=str, required=False, default="/Volumes/MacBackup/hotdog/hot-dog-not-hot-dog/imagenet",  help="path to root where all downloaded images will be stored")

    args = vars(ap.parse_args())
    root_path = args['output_path']

    links = [
        'http://image-net.org/api/text/imagenet.synset.geturls?wnid=n01318894',
        'http://image-net.org/api/text/imagenet.synset.geturls?wnid=n03405725',
        'http://image-net.org/api/text/imagenet.synset.geturls?wnid=n07942152',
        'http://image-net.org/api/text/imagenet.synset.geturls?wnid=n00021265',
        'http://image-net.org/api/text/imagenet.synset.geturls?wnid=n07690019',
        'http://image-net.org/api/text/imagenet.synset.geturls?wnid=n07865105',
        'http://image-net.org/api/text/imagenet.synset.geturls?wnid=n07697537']

    paths = ['pets', 'furniture', 'people', 'food', 'frankfurter', 'chili-dog', 'hotdog']

    store_raw_images(paths, links)
    removeInvalid(paths)


if __name__ == "__main__":
    main()
