# import the necessary packages
from tensorflow.keras.preprocessing.image import img_to_array
import numpy as np
import cv2

class RGB2GrayPreprocessor:
    def __init__(self):
        pass

    def preprocess(self, image):
        # apply the Keras utility function that correctly rearranges
        # the dimensions of the image
        return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)


if __name__ == '__main__':
    import argparse
    import cv2
    ap = argparse.ArgumentParser()
    ap.add_argument("--image", type=str, required=True, help="full path to image")

    args = vars(ap.parse_args())

    image = args['image']
    unresized_image = cv2.imread(image)

    p = RGB2GrayPreprocessor()

    new_im = p.preprocess(unresized_image)

    cv2.imwrite("orig_image.png", img=unresized_image)
    cv2.imwrite("test_image.png", img=new_im)

