import imutils
import cv2


class AspectAwarePreprocessor:

    def __init__(self, width, height, inter=cv2.INTER_AREA):
        self.width = width
        self.height = height
        self.inter = inter

    def preprocess(self, image):
        # grab the dimensions of the image and then initialize the deltas to use when cropping
        (h, w) = image.shape[:2]
        dW = 0
        dH = 0

        # if the width is smaller than the height, then resize along the width
        # (i.e. the smaller dimension ) and then update the deltas to crop the
        # height to the desired dimension
        if w < h:
            image = imutils.resize(image, width=self.width, inter=self.inter)
            dH = int((image.shape[0] - self.height) / 2.0)
        else:
            # otherwise the height is the smaller dimension so resize
            # along the height and then update the deltas to crop the width
            image = imutils.resize(image, height=self.height, inter=self.inter)
            dW = int((image.shape[1] - self.width) / 2.0)

        # with the image resized, we need to re-grab the width and height, folled
        # by performing hte crop
        (h, w) = image.shape[:2]
        image = image[dH:h - dH, dW:w - dW]

        # finally resize the image to the provided spatial
        # dimensions to ensure out output image is always a
        # fixed size
        # when cropping, due to rounding erros, out image target image dimensions
        # may be off +- one pixel, therefre we call cv2.resize to ensure our output
        # image has the desired width and height.
        image = cv2.resize(image, (self.width, self.height), interpolation=self.inter)
        return image

if __name__ == '__main__':
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument("--image", type=str, required=True, help="full path to image")
    ap.add_argument("--to-size", type=int, required=True, help="resize to square of size by size")

    args = vars(ap.parse_args())

    to_size = args['to_size']
    image = args['image']
    unresized_image = cv2.imread(image)

    p = AspectAwarePreprocessor(to_size, to_size)

    new_im = p.preprocess(unresized_image)

    cv2.imwrite("test_image.png", img=new_im)
