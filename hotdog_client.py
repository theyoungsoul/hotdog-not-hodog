from pyimagesearch.datasets.simpledatasetloader import SimpleDatasetLoader
from pyimagesearch.preprocessing.aspectawarepreprocessor import AspectAwarePreprocessor
from pyimagesearch.preprocessing.imagetoarraypreprocessor import ImageToArrayPreprocessor
from tensorflow.keras.models import load_model
import numpy as np
import argparse
import imutils
import cv2
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import classification_report, confusion_matrix
from tensorflow.keras.utils import to_categorical
import pandas as pd

"""
usage:

--holdout-dataset
/Volumes/MacBackup/derived_dataset/holdout
--to-size
128
--model
best_hotdog_cnn_model_100_200.h5
"""

def holdout_model_evaluation(model, X_holdout, y_holdout):
    # evaluate the network
    print("[INFO] evaluating network...")
    predictions = model.predict(X_holdout, batch_size=batch_size)
    print(classification_report(y_holdout.argmax(axis=1),
        predictions.argmax(axis=1), target_names=le_holdout.classes_))

    y_holdout_hd = []
    pred_hd = []
    for v in y_holdout:
        y_holdout_hd.append(v[0])

    for v in predictions:
        if v[0] > v[1]:
            pred_hd.append(1)
        else:
            pred_hd.append(0)

    cm = confusion_matrix(y_holdout_hd, pred_hd)
    df_cm = pd.DataFrame(
            cm, index=['hotdog', 'not_hotdog'], columns=['hotdog', 'not_hotdog'],
        )
    print(df_cm.head())
    tn = cm[0][0]
    tp = cm[1][1]
    fp = cm[0][1]
    fn = cm[1][0]

    print(f"Holdout Accuracy: {(tp+tn)/(tp+tn+fp+fn)}")

    return predictions


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("--holdout-dataset", type=str, required=True, help="path to holdout dataset.  directory should have hot_dog, not_hot_dog directories")
    ap.add_argument("--to-size", type=int, required=True, help="resize to square of size by size")
    ap.add_argument("--model", type=str, required=True, help="name of model")

    args = vars(ap.parse_args())
    to_size = args['to_size']
    model_name = args['model']

    holdout_dataset_path = args['holdout_dataset']

    batch_size = 32
    epochs = 100

    p1 = AspectAwarePreprocessor(to_size, to_size)
    p2 = ImageToArrayPreprocessor(scale_value=255.0, expand_dims=False)

    dataset_loader = SimpleDatasetLoader([p1, p2])
    (holdout_data, holdout_labels, holdout_img_paths) = dataset_loader.load(holdout_dataset_path, verbose=50)

    le_holdout = LabelEncoder().fit(holdout_labels)
    X_holdout = holdout_data
    y_holdout = to_categorical(le_holdout.transform(holdout_labels), 2)

    model = load_model(model_name)

    predictions = holdout_model_evaluation(model, X_holdout, y_holdout)

    for i, pred in enumerate(predictions):
        cv2_image = cv2.imread(holdout_img_paths[i])

        if holdout_labels[i] == 'hot_dog':
            actual_label = 'HotDog'
        else:
            actual_label = "Not HotDog"

        if pred[0] < pred[1]:
            pred_label = "Not HotDog"
        else:
            pred_label = "HotDog"

        color = (0, 255, 0)
        if actual_label != pred_label:
            print(holdout_img_paths[i])
            color = (0,0,255)
            cv2.putText(cv2_image, f"{pred[0]:.2f}(HD) - {pred[1]:.2f}(NHD  )",
                    (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.9, color, 2)

        cv2.putText(cv2_image, f"{actual_label} - {pred_label}",
                    (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.9, color, 2)
        cv2.imshow("HotDog?", cv2_image)
        cv2.waitKey()

