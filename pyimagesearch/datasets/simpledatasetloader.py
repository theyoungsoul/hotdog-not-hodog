import numpy as np
import cv2
import os
from imutils import paths

class SimpleDatasetLoader:

    def __init__(self, preprocessors=[]):
        # store impage preprocessor
        self.preprocessors = preprocessors

    def load(self, datasetPaths, verbose=-1):
        """
        :param: imagePaths - List of paths to images OR a string to the root of the dataset directory.
                the dataset directory assumes the subfolders hold the label name of the images
        """
        if type(datasetPaths) == str:
            imagePaths = list(paths.list_images(datasetPaths))
        elif type(datasetPaths) == list:
            imagePaths = []
            for datasetPath in datasetPaths:
                imagePaths.extend(list(paths.list_images(datasetPath)))

        # initialize the list of features and labels
        data = []
        labels = []
        image_paths = []

        # loop over the input images
        for (i, imagePath) in enumerate(imagePaths):
            # load the image and extract the class label assuming
            # that our path has the following format
            # /path/to/dataset/{class}/{image}.jpg
            image = cv2.imread(imagePath)
            label = imagePath.split(os.path.sep)[-2]

            # check to see if our processors exist
            if self.preprocessors:
                for p in self.preprocessors:
                    image = p.preprocess(image)

            # treat our processed image as a "feature vector"
            # by updating hte data list followed by the labels
            data.append(image)
            labels.append(label)
            image_paths.append(imagePath)

            # show an update every 'verbose' images
            if verbose > 0 and i > 0 and (i + 1) % verbose == 0:
                print(f"[INFO] process {i + 1}/{len(imagePaths)}")

        if verbose > 0:
            print(f"[INFO] loaded {len(data)} rows")
        return np.array(data), np.array(labels), image_paths
