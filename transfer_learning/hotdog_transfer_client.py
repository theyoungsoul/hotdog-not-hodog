from tensorflow.keras.applications import VGG16
from tensorflow.keras.applications import imagenet_utils
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.preprocessing import LabelEncoder
from pyimagesearch.io.hdf5datasetwriter import HDF5DatasetWriter
from imutils import paths
import numpy as np
import progressbar
import argparse
import random
import os
import pickle
from pathlib import Path
import pandas as pd
import cv2

"""
--image
../datasets/animal_holdout/dogs/dogs_00163.jpg
--model
./output/animals_model.pkl
--output-label-encoder
./output/animals/hdf5/animal_label_encoder.pkl
"""

batch_images = []

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
	help="path to image")
ap.add_argument("-b", "--batch-size", type=int, default=32, required=False,
	help="batch size of images to be passed through network")
ap.add_argument("-m", "--model", required=True, help="path to prediction model")
ap.add_argument("-l", "--output-label-encoder", required=True, help='name of file for label encoder')

args = vars(ap.parse_args())

bs = args['batch_size']
batchImages = []

imagePath = args['image']

f = open(args['model'], "rb")
prediction_model = pickle.loads(f.read())
f.close()

f = open(args['output_label_encoder'], "rb")
label_encoder = pickle.loads(f.read())
f.close()
print(f"LabelEncoder Classes: {label_encoder.classes_}")

feature_extraction_model = VGG16(weights="imagenet", include_top=False)

holdout_path = '/Volumes/MacBackup/derived_dataset/holdout'

all_files = []
p = Path(holdout_path).glob('**/*.jpg')
files = [x for x in p if x.is_file()]

y_holdout = []
for file in files:
	filename = str(file)
	if 'not_hot_dog' in filename:
		y_holdout.append(np.argwhere(label_encoder.classes_ == 'not_hot_dog')[0][0])
	else:
		y_holdout.append(np.argwhere(label_encoder.classes_ == 'hot_dog')[0][0])


for imagePath in files:
	image = load_img(str(imagePath), target_size=(224, 224))
	image = img_to_array(image)

	# preprocess the image by
	# (1) expanding the dimensions
	# (2) subtracting the mean RGB pixel intensity from the ImageNet dataset
	# image = np.expand_dims(image, axis=0)
	image = imagenet_utils.preprocess_input(image)

	batchImages.append(image)

features = feature_extraction_model.predict([batchImages], batch_size=bs)

# reshape the features so that each image is represented by a flattened feature vector
# of the 'MexPooling2D' outputs
features = features.reshape((features.shape[0], 512 * 7 * 7))

preds = prediction_model.predict(features)

cm = confusion_matrix(y_holdout, preds)
df_cm = pd.DataFrame(
	cm, index=['hotdog', 'not_hotdog'], columns=['hotdog', 'not_hotdog'],
)
print(df_cm.head())
tn = cm[0][0]
tp = cm[1][1]
fp = cm[0][1]
fn = cm[1][0]

print(f"Holdout Accuracy: {(tp + tn) / (tp + tn + fp + fn)}")

for i, pred in enumerate(preds):
	cv2_image = cv2.imread(str(files[i]))

	if y_holdout[i] == 0:
		actual_label = 'HotDog'
	else:
		actual_label = "Not HotDog"

	if pred == 0:
		pred_label = "HotDog"
	else:
		pred_label = "Not HotDog"

	color = (0, 255, 0)
	if actual_label != pred_label:
		print(str(files[i]))
		color = (0, 0, 255)

	cv2.putText(cv2_image, f"{actual_label} - {pred_label}",
				(10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.9, color, 2)
	cv2.imshow("HotDog?", cv2_image)
	cv2.waitKey()
