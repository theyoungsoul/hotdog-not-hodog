# USAGE
# python train_hotdog_model.py --db ../datasets/animals/hdf5/features.hdf5 \
#	--model animals.cpickle
# python train_hotdog_model.py --db ../datasets/caltech-101/hdf5/features.hdf5 \
#	--model caltech101.cpickle
# python train_hotdog_model.py --db ../datasets/flowers17/hdf5/features.hdf5 \
#	--model flowers17.cpickle

"""

Best HyperParameters: {'C': 10.0}
evaluating
              precision    recall  f1-score   support

     hot_dog       0.92      0.95      0.93       645
 not_hot_dog       0.93      0.90      0.92       530

    accuracy                           0.93      1175
   macro avg       0.93      0.92      0.92      1175
weighted avg       0.93      0.93      0.92      1175

Saving Model: ./hotdog/hotdog_model.pkl


"""
# import the necessary packages
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
import argparse
import pickle
import h5py

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--db", required=True,
	help="path HDF5 database")
ap.add_argument("-m", "--model", required=True,
	help="path to output model")
ap.add_argument("-j", "--jobs", type=int, default=-1,
	help="# of jobs to run when tuning hyperparameters")
args = vars(ap.parse_args())

db = None
try:
    # open the HDF5 database for reading
    # then determine the index of the training and test split,
    # provided that this data was already shuffled PRIOR to writing it to disk
    db = h5py.File(args['db'], 'r')
    print(f"Labels shape: {db['labels'].shape}")

    i = int(db['labels'].shape[0] * 0.75)

    # define the set of parameters that we want to tune then start a
    # grid search where we evaluate our model for each value of C
    print("Tuning hyperparameters")
    # this set of checks is just to make the GridSearchCV faster
    if 'animals' in args['model']:
        params = {
            'C': [0.1, 1.0]
        }
    elif 'caltech' in args['model']:
        params = {
            'C': [100.0, 1000.0]
        }
    elif 'flower' in args['model']:
        params = {
            'C': [0.1, 1.0]
        }
    elif 'hotdog' in args['model']:
        params = {
            'C': [10.0, 100.0]
        }
    else:
        params = {
            'C': [0.1, 1.0, 10.0, 100.0, 1000.0, 10000.0]
        }


    model = GridSearchCV(LogisticRegression(solver='lbfgs', multi_class='auto'),
                         params, cv=3, n_jobs=args['jobs'])

    # train on 0:75% of the data
    X_train = db['features'][:i]
    y_train = db['labels'][:i]
    model.fit(X_train, y_train )
    print(f"Best HyperParameters: {model.best_params_}")

    # eval model
    print("evaluating")
    X_test = db['features'][i:]
    y_test = db['labels'][i:]
    preds = model.predict(X_test)
    cr = classification_report(y_test, preds, target_names=db['label_names'])
    print(cr)

    # serialize the model to disk
    print(f"Saving Model: {args['model']}")
    f = open(args['model'], "wb")
    f.write(pickle.dumps(model.best_estimator_))
    f.close()
finally:
    if db:
        db.close()
