import imutils
import cv2
from tensorflow.keras.applications import imagenet_utils


class ImagenetUtilsPreprocessor:

    def preprocess(self, image):
        image = imagenet_utils.preprocess_input(image)
        return image

